﻿using AutoMapper;
using Microsoft.Practices.Unity;

namespace MinBy.WebApi.App_Start
{
    public class UnityMapping
    {
        /// <summary>
        /// Maps the different interfaces to appropriate classes
        /// </summary>
        /// <param name="config"><see cref="UnityContainer"/></param>
        public static void Map(UnityContainer config)
        {

            //Setup Automapper
            var autoMapperConfig = new MapperConfiguration(AutoMapperMappings.Map);
            var mapper = autoMapperConfig.CreateMapper();

        }
    }
}