﻿using System.Web.Http;
using Microsoft.Practices.Unity;

namespace MinBy.WebApi.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            #region UnityContainer setup
            // Setup IoC container 
            var container = new UnityContainer();
            UnityMapping.Map(container);
            config.DependencyResolver = new UnityResolver(container);
            #endregion


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
